#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>
#include <error.h>

/*
 * TO-DO:
 * arg-input-eval: continue work
 * analyze: write that shit
 */

#include "headers/macros.h"

#include "headers/handle_track.h"

#include "headers/handle_text.h"

#include "headers/std_calc_v0.2.h"

#include "headers/read_filev2.h"

#include "headers/convert.h"

#include "headers/parse.h"

#include "headers/print_file.h"

#include "headers/arg-input_eval.h"

/*
 * Dialogue: 0,0:02:05.56,0:02:07.85,Default,,0000,0000,0000,,Such bad manners.
 * 
 * 02:05.560 --> 02:07.850
 * Such bad manners.
 * 
 */

int main(int argc, char **argv)
{
	char from[MBUFFER], to[MBUFFER], input_name[MBUFFER * 2], output_name[MBUFFER * 2];
	int err, analyze, comp_mode;
	err = analyze = comp_mode = 0;
	stext *mainText = NULL;
	track *mainTrack = NULL;
	FILE *inputfile = NULL, *outputfile = NULL;
	
	/* inputeval, settings */
	
	err = inputeval(argc, argv, from, to, input_name, output_name, &analyze, &comp_mode);
	ERROR_HANDLE(err);
	
	/* reading stuff in */
	
	inputfile = fopen(input_name, "r");
	if (inputfile == NULL)
	{
		err = 3;
		ERROR_HANDLE(err);
	}
	
	err = readFile(inputfile, &mainText);
	ERROR_HANDLE(err);
	
	fclose(inputfile);
	
	/* processing */
	
	if (strcmp(from, "ass") == 0)
	{
		err = parseASS(mainText, &mainTrack);
		ERROR_HANDLE(err);
	}
	if (strcmp(from, "vtt") == 0)
	{
		//err = parseVTT(inputString, &mainTrack);
		ERROR_HANDLE(err);
	}
	
	delete_text(mainText);
	
	/* analyze */
	
	if (analyze)
	{
		
	}
	
	/* print tha files */
	
	outputfile = fopen(output_name, "w+");
	if (outputfile == NULL)
	{
		err = 3;
		ERROR_HANDLE(err);
	}
	
	if (strcmp(to, "ass") == 0)
	{
		printOutputASS(outputfile, mainTrack);
	}
	if (strcmp(to, "vtt") == 0)
	{
		printOutputVTT(outputfile, mainTrack);
	}
	
	fclose(outputfile);
	
	/* freeeee */
	
	delete_track(mainTrack);
	
	return 0;
}
