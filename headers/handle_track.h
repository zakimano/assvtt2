#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#include "macros.h"

/*
 * Header file for track handling
 */

typedef struct s_track
{
	struct s_track* prev;
	char type[MBUFFER];
	unsigned int start_time;
	unsigned int stop_time;
	char comment[MBUFFER];
	int R;
	int G;
	int B;
	char text[TEXT_BUFFER];
	struct s_track* next;
} track;

track* create_track(char type[], unsigned int start_time, unsigned int stop_time, char comment[], int R, int G, int B, char text[])
{
	track* newTrack = malloc(sizeof(track));
	if (newTrack != NULL)
	{
		newTrack->prev = NULL;
		memcpy(newTrack->type, type, strlen(type));
		newTrack->start_time = start_time;
		newTrack->stop_time = stop_time;
		memcpy(newTrack->comment, comment, strlen(comment));
		newTrack->R = R;
		newTrack->G = G;
		newTrack->B = B;
		memcpy(newTrack->text, text, strlen(text));
		newTrack->next = NULL;
	}
	return newTrack;
}

void delete_track(track *oldTrack)
{
	if (oldTrack->next != NULL)
	{
		delete_track(oldTrack->next);
	}
	free(oldTrack);
}

track* add_track(track *trackListLast, char type[], unsigned int start_time, unsigned int stop_time, char comment[], int R, int G, int B, char text[])
{
	track* newTrack = create_track(type, start_time, stop_time, comment, R, G, B, text);
	if (newTrack != NULL)
	{
		trackListLast->next = newTrack;
		newTrack->prev = trackListLast;
	}
	return newTrack;
}

void remove_track(track *toRemove)
{
	track *prevTrack;
	track *nextTrack;
	prevTrack = toRemove->prev;
	nextTrack = toRemove->next;
	prevTrack->next = nextTrack;
	nextTrack->prev = prevTrack;
	free(toRemove);
}
