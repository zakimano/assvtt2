#pragma once


// MACROS

#define TEXT_BUFFER 3000
#define MBUFFER 100

#define HINC 1000
#define HBUF 10

#define ERROR_HANDLE(err) ({\
	if (err != 0) {\
		if (mainTrack != NULL)\
			delete_track(mainTrack);\
		if (mainText != NULL)\
			delete_text(mainText);\
		return err;}\
	})
