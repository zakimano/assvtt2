#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#include "handle_track.h"

#include "std_calc_v0.2.h"

#include "macros.h"

/*
 * Header file for the convert functions
 */

// char type[], unsigned int start_time, unsigned int stop_time, char comment[], int R, int G, int B, char text[]

void c_time(char c_from[], unsigned int *start_time, unsigned int *stop_time, char t_start_time[], char t_stop_time[])
{
	
	/*
	 * 0:02:05.56
	 * 0:02:07.85
	 * 
	 * 02:05.560 --> 02:07.850
	 * 
	 */
	
	unsigned int h, m, s, ms;
	h = m = s = ms = 0;
	
	if (strcmp(c_from, "ass") == 0)
	{
		sscanf(t_start_time, "%d:%d:%d.%d", &h, &m, &s, &ms);
		
		m += h * 60;
		s += m * 60;
		ms += s * 1000;
		
		*start_time = ms;
		
		sscanf(t_stop_time, "%d:%d:%d.%d", &h, &m, &s, &ms);
		
		m += h * 60;
		s += m * 60;
		ms += s * 1000;
		
		*stop_time = ms;
	}
	if (strcmp(c_from, "vtt") == 0)
	{
		
		
		//m += h * 60;
		s += m * 60;
		ms += s * 1000;
		
		*start_time = ms;
		
		
		//m += h * 60;
		s += m * 60;
		ms += s * 1000;
		
		*stop_time = ms;
	}
}

void c_time_text(char c_to[], unsigned int time, char *text)
{
	char buffer[MBUFFER];
	unsigned int h, m, s, ms;
	h = m = s = ms = 0;
	
	/*
	 * 125560
	 * 02:05.560 / 0:02:05.56
	 */
	
	if (strcmp(c_to, "ass") == 0)
	{
		ms = time;
		s = ms / 1000;
		m = s / 60;
		h = m / 60;
		m %= 60;
		s %= 60;
		ms %= 1000;
		
		sprintf(buffer, "%d:%.2d:%.2d.%.2d", h, m, s, ms);
	}
	if (strcmp(c_to, "vtt") == 0)
	{
		ms = time;
		s = ms / 1000;
		m = s / 60;
		//h = m / 60;
		m %= 60;
		s %= 60;
		ms %= 1000;
		
		sprintf(buffer, "%.2d:%.2d.%.2d", m, s, ms);
	}
	
	memcpy(text, buffer, strlen(buffer) + 1);
	
}

void c_text_text(char c_to[], char *stext, char **dtext)
{
	char *buffer;
	
	if (strcmp(c_to, "ass") == 0)
	{
		str_find_replace(stext, &buffer, "\n", "\\N", TEXT_BUFFER);
	}
	if (strcmp(c_to, "vtt") == 0)
	{
		str_find_replace(stext, &buffer, "\\N", "\n", TEXT_BUFFER);
		str_find_omit(buffer, &buffer, '{', '}', TEXT_BUFFER);
	}
	
	*dtext = buffer;
	
}
