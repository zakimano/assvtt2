#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

/*
 * Header file for input and argument evaluation
 */


#include "handle_track.h"

#include "std_calc_v0.2.h"

#include "macros.h"


int inputeval(int margc, char **margv, char *from, char *to, char *input_name, char *output_name, int *analyze, int *comp_mode)
{
	int i;
	
	for (i = 1; i < margc; i++)
	{
		if (strcmp(margv[i], "ft") == 0)
		{
			if (i + 2 <= margc)
			{
				memcpy(from, margv[i + 1], clamp(strlen(margv[i + 1]), 0, MBUFFER - 1));
				from[(int)clamp(strlen(margv[i + 1]), 0, MBUFFER - 1 + 1)] = '\0';
				memcpy(to, margv[i + 2], clamp(strlen(margv[i + 2]), 0, MBUFFER - 1));
				to[(int)clamp(strlen(margv[i + 1]), 0, MBUFFER - 1 + 1)] = '\0';
			}
			else
			{
				// DEBUG
			}
		}
		else if (strcmp(margv[i], "i") == 0)
		{
			if (i + 1 <= margc)
			{
				memcpy(input_name, margv[i + 1], clamp(strlen(margv[i + 1]), 0, MBUFFER * 2 - 1));
				input_name[(int)clamp(strlen(margv[i + 1]), 0, MBUFFER - 1 + 1)] = '\0';
			}
			else
			{
				// DEBUG
			}
		}
		else if (strcmp(margv[i], "o") == 0)
		{
			if (i + 1 <= margc)
			{
				memcpy(output_name, margv[i + 1], clamp(strlen(margv[i + 1]), 0, MBUFFER * 2 - 1));
				output_name[(int)clamp(strlen(margv[i + 1]), 0, MBUFFER - 1 + 1)] = '\0';
			}
			else
			{
				// DEBUG
			}
		}
		else if (strcmp(margv[i], "cm") == 0)
		{
			*comp_mode = 1;
		}
		else if (strcmp(margv[i], "analyze") == 0)
		{
			*analyze = 1;
		}
		else
		{
			// DEBUG
		}
	}
	
	return 0;
}
