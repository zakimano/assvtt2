#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#include "handle_track.h"

#include "convert.h"

#include "macros.h"

void printOutputASS(FILE *outputfile, track *trackListToPrint)
{
	// OLD
	track *iter = trackListToPrint;
	char startb[MBUFFER], stopb[MBUFFER];
	
	/*
	 * Dialogue: 0,0:02:05.56,0:02:07.85,Default,,0000,0000,0000,,Such bad manners.
	 */
	
	while (iter != NULL)
	{
		c_time_text("ass", iter->start_time, startb);
		c_time_text("ass", iter->stop_time, stopb);
		
		fprintf(outputfile, "%s,", iter->type);
		fprintf(outputfile, "%s,%s,", startb, stopb);
		fprintf(outputfile, "%s,,%4d,%4d,%4d,,%s\n", iter->comment, iter->R, iter->G, iter->B, iter->text);
		iter = iter->next;
	}
	
}


void printOutputVTT(FILE *outputfile, track *trackListToPrint)
{
	track *iter = trackListToPrint;
	char startb[MBUFFER], stopb[MBUFFER];
	char *teext;
	
	/*
	 * 02:05.560 --> 02:07.850
	 * Such bad manners.
	 */
	
	fprintf(outputfile, "WEBVTT\n\n");
	while (iter != NULL)
	{
		c_time_text("vtt", iter->start_time, startb);
		c_time_text("vtt", iter->stop_time, stopb);
		
		c_text_text("vtt", iter->text, &teext);
		
		fprintf(outputfile, "%s --> %s\n", startb, stopb);
		fprintf(outputfile, "%s\n\n", teext);
		iter = iter->next;
	}
}
