#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#include "handle_track.h"

#include "handle_text.h"

#include "macros.h"

/*
 * Header file for the function 'readFile'
 */

int readFile(FILE *fIn, stext **textOut)
{
	/* OWN ALLOCATED MEM TO READ INTO */
	
	stext *firstText = NULL;
	stext *lastText = NULL;
	
	unsigned int i = 0;
	
	char buffer[TEXT_BUFFER];
	
	do
	{
		fgets(buffer, TEXT_BUFFER - 1, fIn);
		
		if (i == 0)
		{
			firstText = create_text(buffer);
			if (firstText != NULL)
			{
				lastText = firstText;
			}
			else
			{
				printf("\nERROR: Out of memory\n");
				return 1;
			}
		}
		else
		{
			lastText = add_text(lastText, buffer);
			if (lastText == NULL)
			{
				printf("\nERROR: Out of memory\n");
				delete_text(firstText);
				return 1;
			}
		}
		i++;
		
	} while (!feof(fIn));
	
	//fscanf(fIn, "%s", buffer); formatting-terminated, formatted input!
	
	//fread(buffer, 20, 1, fIn); limit-terminated
	
	//fgets(buffer, 100, fIn); newline-terminated, finite.
	
	//fgetc(fIn); one character
	
	// hit end of file
	
	/* RETURN VALUES HERE */
	
	*textOut = firstText;
	
	return 0;
}
