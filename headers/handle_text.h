#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#include "macros.h"

/*
 * Header file for text handling
 */

typedef struct s_text
{
	struct s_text* prev;
	char line[TEXT_BUFFER];
	struct s_text* next;
} stext;

stext* create_text(char text[])
{
	stext *newText = malloc(sizeof(stext));
	if (newText != NULL)
	{
		newText->prev = NULL;
		memcpy(newText->line, text, strlen(text));
		newText->next = NULL;
	}
	return newText;
}

void delete_text(stext *oldTrack)
{
	if (oldTrack->next != NULL)
	{
		delete_text (oldTrack->next);
	}
	free(oldTrack);
}

stext* add_text(stext *trackListLast, char text[])
{
	stext* newText = create_text(text);
	if ( newText != NULL)
	{
		trackListLast->next = newText;
		  newText->prev = trackListLast;
	}
	return newText;
}

void remove_text(stext *toRemove)
{
	stext *prevText;
	stext *nextText;
	prevText = toRemove->prev;
	nextText = toRemove->next;
	prevText->next = nextText;
	nextText->prev = prevText;
	free(toRemove);
}
