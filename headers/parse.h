#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <stddef.h>
#include <wchar.h>
#include <math.h>
#include <time.h>

#include "handle_track.h"

#include "convert.h"

#include "macros.h"

/*
 * Header file for the function 'parse'
 */


int parseASS(stext *textIn, track **trackOut)
{
	char ttype[MBUFFER];
	char tstart_time[MBUFFER];
	char tstop_time[MBUFFER];
	char tcomment[MBUFFER];
	int tR, tG, tB, throw;
	char ttext[TEXT_BUFFER];
	
	unsigned int start_time, stop_time, i = 0;
	
	track* firstTrack;
	track* lastTrack;
	
	stext *cursor;
	
	/*
	 * Dialogue: 0,0:02:05.56,0:02:07.85,Default,,0000,0000,0000,,Such bad manners.
	 * 
	 * Dialogue: 0,
	 * 0:02:05.56,
	 * 0:02:07.85,
	 * Default,
	 * ,
	 * 0000,
	 * 0000,
	 * 0000,
	 * ,
	 * Such bad manners.
	 */
	
	cursor = textIn;
	
	while(cursor != NULL)
	{
		if (strstr(cursor->line, "Dialogue: "))
		{
			//%d,%s,%s,%[^\t\n],,%d,%d,%d,,%[^\n]
			sscanf(cursor->line, "Dialogue: %d, %[^\n,], %[^\n,], %[^\n,] ,,%d,%d,%d,, %[^\n]", &throw, tstart_time, tstop_time, tcomment, &tR, &tG, &tB, ttext);
			
			c_time("ass", &start_time, &stop_time, tstart_time, tstop_time);
			
			if (i == 0)
			{
				firstTrack = create_track(ttype, start_time, stop_time, tcomment, tR, tG, tB, ttext);
				if (firstTrack != NULL)
				{
					lastTrack = firstTrack;
				}
				else
				{
					printf("\nERROR: Out of memory\n");
					return 1;
				}
			}
			else
			{
				lastTrack = add_track(lastTrack, ttype, start_time, stop_time, tcomment, tR, tG, tB, ttext);
				if (lastTrack == NULL)
				{
					printf("\nERROR: Out of memory\n");
					delete_track(firstTrack);
					return 1;
				}
			}
			
			i++;
		}
		
		cursor = cursor->next;
	}
	
	/*
	
	
	
	*/
	
	/* trackOut should store the first member of the list */
	
	*trackOut = firstTrack;
	
	return 0;
}



int parseVTT()
{
	
	/*
	 * 00:11.640 --> 00:12.610
	 * I 'm scared...
	 * 
	 * 00:11.640 -->
	 * 00:12.610
	 * I 'm scared...
	 * 
	 * Dialogue: 0,0:00:11.64,0:00:12.61,Default,,0000,0000,0000,,I 'm scared...
	 * 
	 */
	
	return 0;
}

